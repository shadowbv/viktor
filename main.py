from docx import Document
from docx.shared import Pt
import sys
import os
from PyQt5.QtWidgets import QApplication, QDialog, QFileDialog, QMessageBox
# from PyQt5.QtCore import pyqtSlot
from choose_dialog import Ui_Form
from docx.enum.text import WD_ALIGN_PARAGRAPH


class Dialog(QDialog):
    def __init__(self, parent=None):
        super(Dialog, self).__init__(parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.setWindowTitle("Для Віктора!")
        self.filename = ""
        self.ui.pushButton.clicked.connect(self.chose_file)
        self.ui.pushButton_2.clicked.connect(self.execute)
        # write_mod_text("test1.docx")  # for testing only

    # @pyqtSlot()
    def chose_file(self):
        self.filename, _ = QFileDialog.getOpenFileName(self, 'Виберіть файл ', ' ',
                                                       "Файл MS Word 2007 (*.docx)")

        self.ui.lineEdit.setText(self.filename)

    def execute(self):
        self.filename
        if self.filename:
            if write_mod_text(self.filename):
                message = QMessageBox(self)
                message.setIcon(QMessageBox.Information)
                message.setWindowTitle("Виконано")
                message.setText("Відкрийте файл: output.docx")
                message.exec()
            else:
                message = QMessageBox(self)
                message.setIcon(QMessageBox.Critical)
                message.setWindowTitle("Помилка")
                message.setText("Закрийте файл: output.docx!")
                message.exec()
        else:
            message = QMessageBox(self)
            message.setIcon(QMessageBox.Critical)
            message.setWindowTitle("Помилка")
            message.setText("Виберіть файл!")
            message.exec()


def write_mod_text(file_name):
    letters = ['А', 'а', 'Е', 'е', 'Є', 'є', 'И', 'и', 'І', 'і', 'Ї', 'ї', 'О', 'о', 'У', 'у', 'Ю', 'ю', 'Я', 'я']
    punctuations = ['.', ',', ':', ';', "'", '!', '?', "`", '(', ')', '[', ']', '=', "\\", '%', '@', '#', '$', '"',
                    '\'', '/', '*', '&', '|', '+', '~', '<', '>', '-', '«', '»']
    numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]

    input_file = file_name
    output_file = "output.docx"

    input_document = Document(input_file)
    paragraphs = input_document.paragraphs

    output_document = Document()
    style = output_document.styles['Normal']
    font = style.font
    font.name = 'Times New Roman'
    font.size = Pt(12)

    for paragraph in paragraphs:
        text = ""
        splited_text = paragraph.text.split()
        # print(splited_text)
        for word in splited_text:
            if len(unpunctuate_word(word, punctuations)) < 3:
                text += word + " "

            elif len(unpunctuate_word(word, punctuations)) == 3:
                tmp_word = ""
                index = 0
                for letter in word:
                    if letter in (punctuations or numbers):
                        tmp_word += letter
                        index = 0
                    else:
                        if index == 1:
                            tmp_word += "_"
                        else:
                            tmp_word += letter
                    index += 1
                text += tmp_word + " "
            else:
                tmp_word = ""
                index = 1
                # print(word)
                for letter in word:
                    # print(index)
                    if letter in (punctuations or numbers):
                        tmp_word += letter
                        index = 1
                    else:
                        # if word[-1] in punctuations:
                        #     index = 1
                        if index % 3 == 0:  # and letter != word[-1]:
                            tmp_word += "_"
                            index = 1
                        else:
                            tmp_word += letter
                            index += 1
                text += tmp_word + " "
                #     if letter in letters:
                #         tmp_word += "_"
                #     else:
                #         tmp_word += letter
                # text += tmp_word + " "
                # # print(word)

        # output_document.add_paragraph(paragraphs[0].text)
        # print(text)
        paragraph = output_document.add_paragraph(text)
        paragraph.alignment = WD_ALIGN_PARAGRAPH.JUSTIFY
    try:
        output_document.save(output_file)
        print("Done!")
        return True
    except os.error as e:
        print("{}: Close document and try again!".format(e.args[1]))
        return False


def unpunctuate_word(word, punctuations):
    unpunctuated_word = ''
    for letter in word:
        if letter not in punctuations:
            unpunctuated_word += letter
    return unpunctuated_word


if __name__ == "__main__":
    # print(argv)
    app = QApplication(sys.argv)
    dialog = Dialog()
    dialog.show()
    sys.exit(app.exec_())
